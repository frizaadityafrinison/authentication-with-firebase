import { Button } from "react-bootstrap";
import { useNavigate } from "react-router";
import { useUserAuth } from "../context/userAuthContext";
import { useState, useEffect } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { query, collection, where, getDocs } from "firebase/firestore";
import { db, auth } from "../firebase-config";

const Home = () => {
  const [name, setName] = useState("");
  const [user, loading] = useAuthState(auth);
  const { logOut } = useUserAuth();
  const navigate = useNavigate();

  const handleLogout = async () => {
    try {
      await logOut();
      navigate("/");
    } catch (error) {
      console.error(error);
    }
  };

  const getData = async () => {
    try {
      const q = query(collection(db, "users"), where("uid", "==", user?.uid));
      const doc = await getDocs(q);
      const data = doc.docs[0].data();

      setName(data.name);
    } catch (err) {
      console.log(err);
      alert("error while fetching data");
    }
  };

  useEffect(() => {
    if (loading) return;
    getData();
  }, [user, loading]);

  return (
    <>
      <div className="p-4 box mt-3 text-center">
        Welcome {name}
        <br />
        {user?.email}
        <br />
      </div>
      <div className="d-grid gap-2">
        <Button variant="primary" onClick={handleLogout}>
          Log out
        </Button>
      </div>
    </>
  );
};

export default Home;
