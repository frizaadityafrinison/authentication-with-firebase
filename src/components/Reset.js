import { Form, Button } from "react-bootstrap";
import { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { auth } from "../firebase-config";
import { useUserAuth } from "../context/userAuthContext";
import { useAuthState } from "react-firebase-hooks/auth";

function Reset() {
  const [email, setEmail] = useState("");
  const [user, loading, error] = useAuthState(auth);
  const { resetPassword } = useUserAuth();
  const navigate = useNavigate();

  useEffect(() => {
    if (loading) return;
    if (user) navigate("/");
  }, [user, loading]);

  async function handleReset(event) {
    event.preventDefault();
    try {
      await resetPassword(email);
    } catch (err) {
      console.log(err.message);
    }
  }

  return (
    <>
      <div className="p-4 box">
        <h2 className="mb-3">Reset Password</h2>
        <Form onSubmit={handleReset}>
          <Form.Group className="mb-3" controlId="formBasicName">
            <Form.Control
              type="email"
              placeholder="Your Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Group>
          <div className="d-grid gap-2">
            <Button variant="primary" type="Submit">
              Send
            </Button>
          </div>
        </Form>
      </div>
      <div className="p-4 box mt-3 text-center">
        Already have an account? <Link to="/">Log In</Link>
      </div>
    </>
  );
}

export default Reset;
