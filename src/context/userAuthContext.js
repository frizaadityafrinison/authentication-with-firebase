import { createContext, useContext, useEffect, useState } from "react";
import { auth, db } from "../firebase-config";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
  GoogleAuthProvider,
  signInWithPopup,
  sendPasswordResetEmail
} from "firebase/auth";
import {
  query,
  getDocs,
  collection,
  where,
  addDoc
} from 'firebase/firestore'

const userAuthContext = createContext();
const googleAuthProvider = new GoogleAuthProvider();

export function UserAuthContextProvider({ children }) {
  const [user, setUser] = useState({});

  function logIn(email, password) {
    return signInWithEmailAndPassword(auth, email, password);
  }

  async function signUp(name, email, password) {
    try {
      const res = await createUserWithEmailAndPassword(auth, email, password);
      const user = res.user;

      await addDoc(collection(db, "users"), {
        uid: user.uid,
        name,
        authProvider: "local",
        email,
      });
    } catch (err) {}
  }

  async function googleSignIn() {
    const res = await signInWithPopup(auth, googleAuthProvider)
    const user = res.user
    const data = collection(db, "users")
    const q = query(data, where("uid", "==", user.uid));
    const doc = await getDocs(q)

    if (doc.docs.length === 0) {
      await addDoc(collection(db, 'users'), {
        uid: user.uid,
        name: user.displayName,
        authProvider: 'google',
        email: user.email
      })
    }
  }

  async function resetPassword(email) {
    try {
      await sendPasswordResetEmail(auth, email)
      alert('Password reset link sent!')
    } catch(err) {
      console.error(err);
      alert(err.message)
    }
  }

  function logOut() {
    return signOut(auth);
  }

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (currentuser) => {
      setUser(currentuser);
    });

    return () => {
      unsubscribe();
    };
  }, []);

  return (
    <userAuthContext.Provider
      value={{ user, logIn, signUp, logOut, googleSignIn, resetPassword }}
    >
      {children}
    </userAuthContext.Provider>
  );
}

export function useUserAuth() {
  return useContext(userAuthContext);
}
